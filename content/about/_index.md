+++
date = "2016-10-10T19:28:09+02:00"
description = ""
title = "About me"

+++

My name is Primož and I am 27-year-old full-stack Web Developer from Slovenia.

I have a bachelor's degree in Computer Science and Informatics.

Currently I work at Koofr, safe storage for files with options to connect multiple cloud accounts.

Posts on this page are kind of my personal notebook, so they're probably very rough and unedited.
