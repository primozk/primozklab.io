+++
date = "2016-12-26T10:10:53+02:00"
description = "Quick guide to publish your Hugo-based webpage on the Internet."
title = "How To Publish Hugo Website To Gitlab Pages"
tags = ["hugo", "gitlab"]

categories = [
  "Development"
]
+++

This is a quick guide showing you how to deploy your website to Gitlab Pages.

### Let's get busy

- Create Gitlab account at https://gitlab.com/
- Fork this project: https://gitlab.com/pages/hugo


Go to your projects settings (*Edit settings*) and edit the `Project name` and `Path` to *`username.gitlab.io`*. Change *`username`* with your Gitlab username. 

- Run this command in your terminal (use your username):

```
$ git clone https://gitlab.com/username/username.gitlab.io.git
```

> If git is not installed: `sudo apt-get install git`

Change the name of the cloned project directory to `username.gitlab.io`.

If you have not configured git on your computer, do that now with: 

```
$ git config user.email "your@email.com"
$ git config user.name "Your Name"
```

Open `config.toml` which is inside of your project and change the `baseurl` value to `baseurl = "https://username.gitlab.io/"`

You are now prepared to commit and push changes to your project.

```
$ git push origin master
```

Fill in your login info if requested and go to `https://username.gitlab.io/`.
If your page is not visible yet, wait a couple of minutes, because it may take some time to show up.
