+++
date = "2019-03-19T21:00:00+02:00"
description = "Do not return errors with status code 200."
title = "When HTTP 200 OK from API endpoint returned error"
tags = []
categories = [
 "REST",
 "API"
]
+++
## Fun at work
Today I was sending data to some remote API endpoint and it kept returning HTTP response with code 200 OK. This is normally a good sign, but this was it's content:

```
{"error": "not present"}
```

OK, so I thought maybe the API always returns 200 and errors in it's content. This is bad practice but I've seen it a lot in many APIs (even from Microsoft). But usually if an object (in a database) is not found, you return 404 not 200.

However, this was not the case here because I asked them what this error means and why are they returning it with 200 OK.

The answer shocked me:

They responded that this is normal behaviour and it actually means that **no errors are present**.
 
Why would anyone put this in the 200 response?
 