+++
date = "2017-03-21T20:00:00+02:00"
description = "We go through some of the reasons why you should start blogging."
title = "Why am I starting my own blog?"
tags = []

categories = [
  "Blog"
]
+++

## Learn all the things!

There are so many great technical blogs out there, that starting blogging is scaring me. Why should I write about anything and make it public?

#### Improve communication
I want to learn new things and by having my own blog, I think I will learn not just writing and improving my English skills, but much more.

#### Be better developer
By learning all the technical stuff that is necessary to run a blog, starting from infrastructure to host a blog, SEO and in the end how to design a blog site by their own preferences, everyone can 
become a better full-stack web-developer. Or choose only one side of the stack - whichever has cookies.

#### Learn how to network and promote
I want to go through the pain of promoting a blog and see how much effort is needed to make my name known to at least some people, beside NSA, that don’t know me personally, yet.

#### Personal satisfaction of accomplishment
My posts will be kept as a personal journal about every little thing that I have learned in my career and personal life and blogging will keep my mind busy and push my professional development.

#### Taking the next step
I hope this is all just a foundation for my first talk someday because my goal is to be a speaker at least once and push myself out of my comfort zone.

If any learning creature stumbles upon my findings and appreciates them, it will be my biggest victory.
