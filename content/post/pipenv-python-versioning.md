+++
date = "2018-11-27T20:00:00+02:00"
description = "What to do if Pipenv does not support custom options"
title = "Try this if your Python package installation fails"
tags = []

categories = [
  "Python",
  "Pipenv"
]
+++

## Problem

Ubuntu 18.04 comes with Python 3.6 preinstalled. Recently I checked out a private project that uses Pipfile and the project requires Python 3.5. 

```
[requires]
python_version = "3.5"
```

I tried to install Django Compressor, but it failed because `rjsmin` and `rcssmin` could not be installed. I've found an existing [issue](https://github.com/django-compressor/django-compressor/issues/807) which suggested installing the package with 
```
pip install rjsmin --install-option="--without-c-extensions"
```

But Pipenv does not support this flag (at least not that I would know). And also it would make Compressor much slower. So instead I installed `python3.5-dev`:

```
sudo apt install python3.5-dev
```

and run

```
pipenv install django-compressor
```

again. It should install properly without any exceptions.

## Conclusion

If any installation of Python packages fails, try installing Python header files for the correct version of Python that the virtual environment is using.